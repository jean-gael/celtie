<?php

namespace App\Entity;

use App\Repository\RoyaumeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoyaumeRepository::class)
 */
class Royaume
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Royaume;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Religion;

    /**
     * @ORM\OneToMany(targetEntity=Person::class, mappedBy="royauĂme")
     */
    private $people;

    public function __construct()
    {
        $this->people = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoyaume(): ?string
    {
        return $this->Royaume;
    }

    public function setRoyaume(string $Royaume): self
    {
        $this->Royaume = $Royaume;

        return $this;
    }

    public function getReligion(): ?string
    {
        return $this->Religion;
    }

    public function setReligion(?string $Religion): self
    {
        $this->Religion = $Religion;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPeople(): Collection
    {
        return $this->people;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->people->contains($person)) {
            $this->people[] = $person;
            $person->setRoyauĂme($this);
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->people->removeElement($person)) {
            // set the owning side to null (unless already changed)
            if ($person->getRoyauĂme() === $this) {
                $person->setRoyauĂme(null);
            }
        }

        return $this;
    }
}
