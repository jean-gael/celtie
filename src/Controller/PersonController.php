<?php

namespace App\Controller;

use App\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PersonController extends AbstractController
{
    const PERPAGE = 5;

    /**
     * @Route("/person", name="person")
     */
    public function index(Request $request): Response
    {
        $curPage = ($request->get('page')) ? $request->get('page') : 1;
        
        $personRepo = $this->getDoctrine()->getRepository(Person::class);
        $persons = $personRepo->findAllPaginated($curPage, self::PERPAGE);      

        # Pagination 
        $numAllPerson = $personRepo->count();
        $numPage = ceil($numAllPerson/self::PERPAGE);
dump($curPage);
        return $this->render('person/index.html.twig', [
            'persons' =>$persons,
            'curPage' => $curPage,
            'numPage' => $numPage,
            'controller_name' => 'PersonController',
        ]);
    }
}
