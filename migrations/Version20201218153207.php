<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201218153207 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD1767012EBF1');
        $this->addSql('DROP INDEX IDX_34DCD1767012EBF1 ON person');
        $this->addSql('ALTER TABLE person CHANGE `royau?me_id` royaume_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176A3878AD1 FOREIGN KEY (royaume_id) REFERENCES royaume (id)');
        $this->addSql('CREATE INDEX IDX_34DCD176A3878AD1 ON person (royaume_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD176A3878AD1');
        $this->addSql('DROP INDEX IDX_34DCD176A3878AD1 ON person');
        $this->addSql('ALTER TABLE person CHANGE royaume_id royau?me_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1767012EBF1 FOREIGN KEY (royau?me_id) REFERENCES royaume (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_34DCD1767012EBF1 ON person (royau?me_id)');
    }
}
